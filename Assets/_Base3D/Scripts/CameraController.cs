﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private List<CinemachineVirtualCamera> _cameras = new List<CinemachineVirtualCamera>();
    private float testVariable = 0.0f;
    private void Awake()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            _cameras.Add(transform.GetChild(i).GetComponent<CinemachineVirtualCamera>());
        }
    }

    void Start()
    {
        EventsManager.Instance.ActionActivateCamera += OnActivateCamera;
    }

    private void OnDestroy()
    {
        EventsManager.Instance.ActionActivateCamera -= OnActivateCamera;
    }

    private void OnActivateCamera(int cameraNumber)
    {
        DeactivateAllCameras();
        _cameras[cameraNumber].Priority = 10;
    }

    private void DeactivateAllCameras()
    {
        foreach (var cinemachineVirtualCamera in _cameras)
        {
            cinemachineVirtualCamera.Priority = 9;
        }
    }
}
